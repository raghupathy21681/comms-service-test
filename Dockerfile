FROM node:14.15.3
WORKDIR /app
COPY package.json /app
COPY . /app
RUN pwd
RUN npm install


EXPOSE 3005

CMD ["npm", "run", "contrun"]
